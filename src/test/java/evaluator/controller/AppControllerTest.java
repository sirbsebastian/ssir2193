package evaluator.controller;

import evaluator.exception.DuplicateException;
import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.exception.NotAbleToCreateTestException;
import evaluator.model.Question;
import evaluator.model.Quiz;
import evaluator.model.Statistic;
import org.junit.Test;

import static org.junit.Assert.*;

public class AppControllerTest {

    //teste functionaliteatea 1
    @Test(expected = DuplicateException.class)
    public void addQuestionTest1() throws Exception{
        AppController appController=new AppController();
        appController.addNewQuestion(new Question("In ce zi ne aflam?","1)Sambata","2)Duminica","3)Luni","3","Zile"));

    }



    //teste functionalitatea 2
    //nu am incarcat intrebarile din fisier
    @Test(expected = NotAbleToCreateTestException.class)
    public void createNewTest() throws Exception {
        AppController appController = new AppController();
        appController.createNewTest();

    }
    //valid
    @Test
    public void createNewTest2() throws Exception{
        AppController appController = new AppController();
        appController.loadQuestionsFromFile("intrebari.txt");
        Quiz q=appController.createNewTest();
        assertEquals(q.getQuestions().size(),5);
    }
    //nu exista 5 domenii
    @Test(expected = NotAbleToCreateTestException.class)
    public void createNewTest3() throws Exception{
        AppController appController = new AppController();
        appController.loadQuestionsFromFile("intrebari2.txt");
        appController.createNewTest();
    }
    //teste functionalitatea 3
    @Test
    public void statisticTest1() throws Exception{
        AppController appController = new AppController();
        appController.loadQuestionsFromFile("intrebari.txt");
        Statistic statistic =appController.getStatistic();
        assert(statistic.getDomainQuestions().size()==5);

    }
    @Test(expected = NotAbleToCreateStatisticsException.class)
    public void statisticTest2() throws Exception{
        AppController appController = new AppController();
        appController.loadQuestionsFromFile("intrebari3.txt");
        Statistic statistic =appController.getStatistic();

    }

}