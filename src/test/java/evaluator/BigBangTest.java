package evaluator;

import evaluator.controller.AppController;
import evaluator.exception.DuplicateException;
import evaluator.exception.InputValidationFailedException;
import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.exception.NotAbleToCreateTestException;
import evaluator.model.Question;
import evaluator.model.Quiz;
import evaluator.model.Statistic;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class BigBangTest {

    private AppController appControllerMock;

    @Before
    public void setUp() throws Exception {
        appControllerMock=new AppController();
    }

    @Test(expected = DuplicateException.class)
    public void addQuestionTest1() throws Exception{
        appControllerMock.addNewQuestion(new Question("In ce zi ne aflam?","1)Sambata","2)Duminica","3)Luni","3","Zile"));

    }

    @Test
    public void createNewTest2() throws Exception{
        appControllerMock.loadQuestionsFromFile("intrebari.txt");
        Quiz q=appControllerMock.createNewTest();
        assertEquals(q.getQuestions().size(),5);
    }

    @Test
    public void statisticTest1() throws Exception{
        appControllerMock.loadQuestionsFromFile("intrebari.txt");
        Statistic statistic =appControllerMock.getStatistic();
        assert(statistic.getDomainQuestions().size()==5);

    }

    @Test(expected = DuplicateException.class)
    public void integrationTest() throws InputValidationFailedException, DuplicateException, IOException, NotAbleToCreateTestException, NotAbleToCreateStatisticsException {
   // try{

        appControllerMock.addNewQuestion(new Question("In ce zi ne aflam?","1)Sambata","2)Duminica","3)Luni","3","Zile"));
        appControllerMock.addNewQuestion(new Question("In ce zi ne aflam maine?","1)Sambata","2)Duminica","3)Luni","3","Zile"));


        appControllerMock.loadQuestionsFromFile("intrebari.txt");
        Quiz q=appControllerMock.createNewTest();
        assertEquals(q.getQuestions().size(),5);


        appControllerMock.loadQuestionsFromFile("intrebari.txt");
        Statistic statistic =appControllerMock.getStatistic();
        assert(statistic.getDomainQuestions().size()==6);



    }
//    catch (Exception e){
//        fail();
//    }
  //  }
}
