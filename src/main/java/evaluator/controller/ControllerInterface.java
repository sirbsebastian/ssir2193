package evaluator.controller;
import evaluator.exception.DuplicateException;
import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.exception.NotAbleToCreateTestException;
import evaluator.model.Question;
import evaluator.model.Quiz;
import evaluator.model.Statistic;

import java.io.IOException;


public interface ControllerInterface {
     Question addNewQuestion(Question question) throws DuplicateException, IOException;
     Quiz createNewTest() throws NotAbleToCreateTestException;
     Statistic getStatistic() throws NotAbleToCreateStatisticsException;

    }


